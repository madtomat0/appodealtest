//
//  ViewController.m
//  AppodealTest
//
//  Created by Paul Gorbunov on 31/05/2018.
//  Copyright © 2018 Lunapp. All rights reserved.
//

#import "ViewController.h"
#import "MTAppodealHelper_iOS.h"
#import "MTDefines_iOS.h"

#define IOS_APPODEAL_API_KEY @""

@interface ViewController ()

@end

@implementation ViewController

- (void) viewDidLoad {
	[super viewDidLoad];
	
	[[MTAppodealHelper_iOS sharedInstance] setupWithAPIKey:IOS_APPODEAL_API_KEY hasConsent:YES];
	[[MTAppodealHelper_iOS sharedInstance] cacheAd:AppodealAdTypeBanner];
	[[MTAppodealHelper_iOS sharedInstance] cacheAd:AppodealAdTypeInterstitial];
	[[MTAppodealHelper_iOS sharedInstance] cacheAd:AppodealAdTypeRewardedVideo];
}

//MARK - Actions

- (IBAction)showBanner:(id)sender {
	[[MTAppodealHelper_iOS sharedInstance] showAdWithStyle:AppodealShowStyleBannerBottom];
}

- (IBAction)hideBanner:(id)sender {
	[[MTAppodealHelper_iOS sharedInstance] hideBanner];
}

- (IBAction)preloadInterstitial:(id)sender {
	[[MTAppodealHelper_iOS sharedInstance] cacheAd:AppodealAdTypeInterstitial];
}

- (IBAction)showInterstitial:(id)sender {
	if ([[MTAppodealHelper_iOS sharedInstance] isReadyForShowWithStyle:AppodealShowStyleInterstitial]) {
		[[MTAppodealHelper_iOS sharedInstance] showAdWithStyle:AppodealShowStyleInterstitial];
	} else {
		MLOG(@"AppodealShowStyleInterstitial is not available");
	}
}

- (IBAction)preloadRewarded:(id)sender {
	[[MTAppodealHelper_iOS sharedInstance] cacheAd:AppodealAdTypeRewardedVideo];
}

- (IBAction)showRewarded:(id)sender {
	[[MTAppodealHelper_iOS sharedInstance] showAdWithStyle:AppodealShowStyleRewardedVideo];
}

@end
