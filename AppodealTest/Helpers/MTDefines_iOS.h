//
//  MTDefines_iOS.h
//  FillTheWords
//
//  Created by Pavel on 20/09/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#ifndef FillTheWords_MTDefines_iOS_h
#define FillTheWords_MTDefines_iOS_h

#if defined (DEBUG)
#	define MLOG(fmt, args...) NSLog (@"%s (%d): " fmt, __PRETTY_FUNCTION__, __LINE__, ##args)
#else
#	define MLOG(...)
#endif

#define BUG_HERE NSLog (@"Reached line %d in %s (%s). Possible bug.", __LINE__, __FILE__, __PRETTY_FUNCTION__);

#endif
