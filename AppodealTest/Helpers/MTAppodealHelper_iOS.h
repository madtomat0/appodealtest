//
//  MTAppodealHelper_iOS.h
//  FillTheWords-mobile
//
//  Created by Pavel on 18/12/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Appodeal/Appodeal.h>

typedef NS_ENUM(NSInteger, MTAppodealAdState) {
    MTAppodealAdStateDidLoad = 0,
    MTAppodealAdStateFailedToLoad,
    MTAppodealAdStateFailedToPresent,
    MTAppodealAdStateWillPresent,
	MTAppodealAdStateDidPresent,
    MTAppodealAdStateDidClick,
    MTAppodealAdStateDidShow,
	MTAppodealAdStateWillDismiss,
    MTAppodealAdStateDidDismiss,
	MTAppodealAdStateVideoDidFinish
};

typedef void(^MTAppodealAdCallback)(MTAppodealAdState state);

@interface MTAppodealHelper_iOS : NSObject

+ (instancetype) sharedInstance;

- (void) setupWithAPIKey:(NSString*) apiKey hasConsent:(BOOL)hasConsent;

- (void) showAdWithStyle:(AppodealShowStyle) showStyle;
- (void) hideBanner;
- (void) cacheAd:(AppodealAdType)adType;
- (BOOL) isReadyForShowWithStyle:(AppodealShowStyle)showStyle;

@end
