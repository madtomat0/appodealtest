//
//  MTAppodealHelper_iOS.m
//  FillTheWords-mobile
//
//  Created by Pavel on 18/12/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#import "MTAppodealHelper_iOS.h"
#import "MTDefines_iOS.h"

@interface MTAppodealHelper_iOS()<AppodealBannerDelegate, AppodealInterstitialDelegate, AppodealRewardedVideoDelegate>
@end

@implementation MTAppodealHelper_iOS

#pragma mark -
#pragma mark Initialization

+ (instancetype) sharedInstance {
	static MTAppodealHelper_iOS* instance;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		MLOG(@"MTAppodealHelper_iOS init");
		instance = [[MTAppodealHelper_iOS alloc] init];
	});
	return instance;
}

- (instancetype) init {
	self = [super init];
	if (self) {
		
	}
	
	return self;
}

#pragma mark -
#pragma mark Setup

- (void) setupWithAPIKey:(NSString*) apiKey hasConsent:(BOOL)hasConsent {
	[Appodeal setAutocache:NO types:(AppodealAdTypeInterstitial | AppodealAdTypeRewardedVideo | AppodealAdTypeBanner)];
	[Appodeal setSmartBannersEnabled:YES];
	
	[Appodeal setBannerDelegate:self];
	[Appodeal setInterstitialDelegate:self];
	[Appodeal setRewardedVideoDelegate:self];
	
	[Appodeal initializeWithApiKey:apiKey types:AppodealAdTypeBanner | AppodealAdTypeInterstitial | AppodealAdTypeRewardedVideo hasConsent:hasConsent];
}

#pragma mark -
#pragma mark General Methods

- (void) showAdWithStyle:(AppodealShowStyle) showStyle {
	UIViewController* rootVC = [UIApplication sharedApplication].keyWindow.rootViewController;
	
	[Appodeal showAd:showStyle rootViewController:rootVC];
}

- (void) hideBanner {
	[Appodeal hideBanner];
}

- (void) cacheAd:(AppodealAdType)adType {
	[Appodeal cacheAd:adType];
}

- (BOOL) isReadyForShowWithStyle:(AppodealShowStyle)showStyle {
	return [Appodeal isReadyForShowWithStyle:showStyle];
}

#pragma mark -
#pragma mark AppodealBannerDelegate

- (void) bannerDidLoadAdIsPrecache:(BOOL)precache {
	MLOG(@">>>");
}

- (void) bannerDidFailToLoadAd {
	MLOG(@">>>");
}

- (void) bannerDidExpired {
	MLOG(@">>>");
}

- (void) bannerDidClick {
	MLOG(@">>>");
}

- (void) bannerDidShow {
	MLOG(@">>>");
}

#pragma mark -
#pragma mark AppodealInterstitialDelegate

- (void) interstitialDidLoadAdIsPrecache:(BOOL)precache {
	MLOG(@">>>");
}

- (void) interstitialDidFailToLoadAd {
	MLOG(@">>>");
}

- (void) interstitialDidExpired {
	MLOG(@">>>");
}

- (void) interstitialDidFailToPresent {
	MLOG(@">>>");
}

- (void) interstitialWillPresent {
	MLOG(@">>>");
}

- (void) interstitialDidDismiss {
	MLOG(@">>>");
}

- (void) interstitialDidClick {
	MLOG(@">>>");
}

#pragma mark -
#pragma mark AppodealRewardedVideoDelegate

- (void) rewardedVideoDidLoadAdIsPrecache:(BOOL)precache {
	MLOG(@">>>");
}

- (void) rewardedVideoDidFailToLoadAd {
	MLOG(@">>>");
}

- (void) rewardedVideoDidExpired {
	MLOG(@">>>");
}

- (void) rewardedVideoDidFailToPresentWithError:(nonnull NSError *)error {
	MLOG(@">>>");
}

- (void) rewardedVideoDidPresent {
	MLOG(@">>>");
}

- (void) rewardedVideoWillDismissAndWasFullyWatched:(BOOL)wasFullyWatched {
	MLOG(@">>>");
}

- (void) rewardedVideoDidFinish:(NSUInteger)rewardAmount name:(nullable NSString *)rewardName {
	MLOG(@">>>");
}

@end
